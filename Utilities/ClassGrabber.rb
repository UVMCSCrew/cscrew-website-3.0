#Ethan Joachim Eldridge
#May 22 12:46AM
#URL Grabber Modifications to work with php
#my plan as of right now is to call exec() on these
#scripts passing in the proper variables from the php
#variables will be validated be regEX

#Passed in variables are the following:
#semester (can be: fall,summer.spring anycase)
#Running this file with no variables is equivalent to asking what this file does.
#Also, this file, when run, will return the class count.  This is because from
#the admin page it will run this and a couple of other scripts, while this file does 
#update the config.php file, this file will most likely be loaded when the scripts are
#being executed, so changing it will do nothing for the scope of the script.

#Are they just asking for help?
if ARGV.length < 1
	puts "This file goes to the internet and grabs the classes from uvm's giraffe server, it expects a single argument: summer,fall, or spring may be passed."
	Kernel.abort
end

#We need open-uri to go grab the files
require 'open-uri'

#Figure out which semester it is (common downcase and no whitespace)
semester = ARGV[0].downcase.strip
#validation using pattern matching
if !semester.match /^(spring|fall|summer)$/ then
	puts "Passed parameter: " << semester << " does not match specification, must be summer,spring, or fall."
	Kernel.abort
end

#We now have whether its Fall or Spring so 
#Construct the url to go get things from!
url_string = 'http://giraffe.uvm.edu/~rgweb/batch/curr_enroll_' << semester << '.txt'
file = open(url_string)

#Read out the webpage (This could take like 10 seconds or so)
contents = file.read

#Match and find the start and beginning of all CS Classes
begin_location = contents =~ /"CS"/

#And find where it ends as well please (yay regex)
end_location = contents =~ /"CSD"/

#Grab out all that data and grab how many lines
going_out = contents[begin_location..end_location]
count = going_out.count "\n"

#Write it out to our classes file
file_name = '../Configuration/Classes' << semester.capitalize << '.txt'
File.open(file_name,'w+') {|fileHandle| fileHandle.write(going_out)}

#Modify the config.php file to have an updated class count
#note: it might be a good idea to have the path to the config file
#stored in the Configuration file itself, and be passed to this script
configContent = ''
File.open('../Configuration/config.php','r') { |fileHandle| configContent = fileHandle.read}

#Find where the class count is defined: note that comments screw this up.
count_start = configContent =~ /\$ClassCount/
count_end = configContent[count_start..(count_start+30)] =~ /\n/
count_end += count_start
equals_start = configContent[count_start..count_end] =~ /=/
count_start += equals_start
configContent[(count_start+1)..count_end] = count.to_s + ";\n"

#Now find and replace the ClassCount = x with ClassCount = our new count
File.open('../Configuration/config.php','w+') { |fileHandle| fileHandle.write(configContent)}
