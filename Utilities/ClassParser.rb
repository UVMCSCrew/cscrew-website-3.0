#Ethan Joachim Eldridge
#1:29AM May 22 2012
#Class Parser modified to work with php.

#This file parses a .txt file created by ClassGrabber.rb
#located at ../Configurtation/Classes(Fall|Spring|Summer).txt
#It requires the following parameters to be passed via command line
#semester, class count. In that order. This file is expected to be run
#via the exec() command from an administrative php page.

#Check if they're asking for help
if ARGV.length < 1
	puts "This file parses the results of running ClassGrabber and creates the file used to update the database login classes table"
	Kernel.abort
end


#Figure out which semester it is (common downcase and no whitespace)
semester = ARGV[0].downcase.strip
#validation using pattern matching
if !semester.match /^(spring|fall|summer)$/ then
	puts "Passed parameter: " << semester << " does not match specification, must be summer,spring, or fall."
	Kernel.abort
end
#place in proper form to read file
semester.capitalize!

#Convert class count to a number
class_count = Integer(ARGV[1].strip)

#Construct the file name based on conventions of ClassGrabber.rb
file_name = '../Configuration/Classes' << semester << '.txt'

#Create something to hold our output, also allocate a little bit of space
outputContent = Array.new(class_count) { |i| "" }

#Open the class file, if it doesn't exist then thats really unfortunate
#and we'll do our best to exit gracefully
begin
	file = File.open(file_name,'r')
	class_count.times do |i|	
		line = file.gets.split(',')
		#Parse out the info we would like: The weird ()-
		outputContent[i] = line[0] + line[1] << '- ' << line[2].delete("()0-9\-") << ' (' << line[-2].strip << ' ' << line[-3] << ") \n"
		outputContent[i].gsub!(/"/,"")
	end
	file.close
rescue => err
	puts "Couldn't open up the file, Exception: #{err}"
	err
end

#remove duplicate entries using hash table
duplicantDestroyer = Hash.new
class_count.times {|i| duplicantDestroyer[outputContent[i]] = i}

outputContent = duplicantDestroyer.keys

#Now write out everything to a file we can love :) 
File.open('../Configuration/LoginClasses.txt','w+') do |handle| 
	(class_count).times { |i| handle.write(outputContent[i]) }
end

#Delete the old file that we don't need anymore
begin
	File.delete file_name
rescue => err
	puts "Couldn't delete the useless csv. Did you run CSClassGrabber yet?\nException #{err}"
	Kernal.abort
end

