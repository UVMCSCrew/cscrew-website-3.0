#Ethan Joachim Eldridge
#Fun Fact! This Utility file was started at 5:40am 2012 5/7/12 before a cs295 Final

#This utility updates the R332_Languages table in the database. It does this by killing
#the contents of the table itself, setting the auto increment back to 1, then adding
#the contents of knownLanguages.txt from the configuration file to the table afterward.
#If you need to change the language table in the database feel free to do it manually, 
#or change the knownLanguages.txt file and run this script!


#First off some help and argument checking

if ARGV.length ==  0
	puts "This function requires a hostname, username, and password to be passed!\n"
	puts "You may also pass a database name if you don't want to read from the one\n"
	puts "Specified in configuration/config.txt"
	Kernel.abort
end

if ARGV.length < 3
	puts %/This script requires host of the database, user name, and\npassword to be passed via command line/
	Kernel.abort
end


require "mysql"

#Assume we're valid for now.
hostname = ARGV[0]
username = ARGV[1]
password = ARGV[2]
database = ARGV[3] if ARGV.length > 3


#we need the database if its not already defined
File.open('../Configuration/config.txt','r') do |fHandle|
	while line = fHandle.gets
		if line =~ /Database/
			lineSplut = line.split('=')
			database ||= lineSplut[1].strip
		end
	end
end

#until testing and website is complete please direct all database to XXX
database = "CSCREW_1"


#now lets load in the purposes to make sure they exist before we try to go and delete and update things
languages = []
begin
	File.open('../Configuration/knownLanguages.txt','r') do |fHandle|
		i = 0
		while line = fHandle.gets
			languages[i] = line.delete("\n")
			i +=1
		end
	end
rescue => awdamn
	puts "Couldn't load the knownLanguages file from the configuration directory and I got an Exception: #{awdamn}"
	Kernel.abort
end


#Now we open the database and attempt to load the right one
begin
	the_db = Mysql.connect hostname, username, password
	the_db.select_db database
rescue => err
	puts "Couldn't connect  to the database! Exception #{err}"
	Kernel.abort
end

#Delete the old values
deleteQuery = "TRUNCATE TABLE `R332_Languages`;"
the_db.query(deleteQuery)

#Now we can alter the increment of the primary keys without worry
alterQuery = "ALTER TABLE  `R332_Languages` AUTO_INCREMENT =1"
the_db.query( alterQuery)

#Now add in our new purposi (If any of these fail I will be baffled)
languages.each do |language|
	insertionQuery = "INSERT INTO `#{database}`.`R332_Languages`(`pkLanguageID`,`Language`) VALUES ( NULL, '#{language}');"
	the_db.query(insertionQuery) if !language.empty?
end

#close the database
the_db.close