#Ethan Joachim Eldridge
#2:00AM May 22 2012
#Database updater for login class table

#This file should be run after the grabber and parser have run once. It accepts
#command line arguments as follows:
#hostname, username, password, database, semester
#
#This file is expected to be ran via php's exec command with the proper validated 
#inputs being passed in. However, semester is validated for safety. File does the
#following:
#
#Populates the login_class table in the database with the information
#stored in the LoginClasses.txt.


#Help Check
if ARGV.length == 0
	puts "You must pass a host, username, password, database and semester via command line to this function!"
	puts "This file is expected to be ran from the admin php page"
	Kernel.abort
end

if ARGV[0].downcase ==  "help"
	#Jesus, I hate not breaking everything up but its alright.
	puts %/This script updates the login_class table in the database. To function you must pass in the hostname, username, password, database, and semester to the database as command line arguments./
	Kernel.abort
end

#Number of argument check
if ARGV.length < 5
	puts "This script requires 5 arguments: hostname,username,password,database name, and semester"
	Kernel.abort
end

#Validate what inputs we can:
if !ARGV[4].downcase.match /^(spring|fall|summer)$/ then
	puts "Passed parameter: " << semester << " does not match specification, must be summer,spring, or fall."
	Kernel.abort
end

#Copy over everything
host = ARGV[0]
user = ARGV[1]
pass = ARGV[2]
database_name = ARGV[3]
semester = ARGV[4].capitalize.strip

#This is the only script in the set ClassGrabber,ClassParser, and loginClassUpdater that
#needs to update the database itself.
require 'mysql'

#until testing and website is complete please direct all database to XXX
database_name = "CSCREW_1"

#Now we open the database and attempt to load the right one
begin
	the_db = Mysql.connect host, user, pass
	the_db.select_db database_name
rescue => err
	puts "Couldn't connect  to the database! Exception #{err}"
	Kernel.abort
end

#Load the new data from the login_classes file (Use hash to make sure of !duplicants)
classesToLoad = Hash.new
counter = 0
File.open('../Configuration/LoginClasses.txt','r') do |fHandle|
	while line = fHandle.gets
		classesToLoad[counter] = line
		counter += 1
	end
end

#snag all the lines into an array
classArray = classesToLoad.values

#figure out which semester was before this one so we can delete it:
prevSemester = ''
if semester =~ /Summer/
	prevSemester = 'Spring'
elsif semester =~ /Fall/
	prevSemester = 'Summer'
elsif semester =~ /Spring/
	prevSemester = 'Fall'
else
	#This line should never occur because of the above checks but safety first
	puts "Semester argument ill defined, does not match Summer,Fall, or Spring"
	Kernel.abort
end


#Remove the old values from the database by grabbing the previous semester (we need to do this first for conflicting cs course numbers)
deleteQuery = "DELETE FROM `login_class` WHERE `login_class`.`Semester` = '#{prevSemester}' ;"
the_db.query(deleteQuery)

#Construct the insert using each line (Note this depends on Parser's write conventions)
classArray.each do |line|
	courseNo = line.split('-')[0][2,3]
	className = line.split('(')[0].split('-')[1].strip
	instructor = line[(line=~/\(/)+1...line=~/\)/].strip
	db_query = "INSERT INTO `#{database_name}`.`login_class` VALUES ('#{courseNo}','#{className}','#{instructor}','#{semester}');"
	#we'll error out if we try to duplicate a class so watch out
	begin
		the_db.query(db_query)
	rescue =>dup
		#do nothing and carry on 
	end
end

#disconnect from the database
the_db.close
