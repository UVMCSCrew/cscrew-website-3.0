<?php
/*************************/
/*Ethan Joachim Eldridge
/*May 3rd 2012
/*
/*Postable interface. 
/*We can use this to enforce common functionality to all things that are
/*able to be posted to the forums (hence postable name). This way we can
/*design decorators and validators that will operate on classes that
/*use this interface
**************************/


//JOSH!!! Help me out with this one please XXXX
interface Postable{
	function getToID();
	function getFromID();
	function getContent();
}

//We could also include the validators and decorators that might work on Postables below here
//Things that double check for injection or malicious intent, make sure that ID's exist in 
//the database (by asking the ForumDB class to help out with that), and etc etc. This would
//probably be a pretty good place to define them I think. line 63 of Entry.php might benefit




?>