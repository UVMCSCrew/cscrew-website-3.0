<?php
/**********************************/
/* Ethan J. Eldridge.  May 30th 2012
/* 
/* Contains the definitions for the abstract class
/* Member, and it's children. The expected use of
/* these classes is that the member will handle (in
/* its constructor) being created from a database result
/* and will be able to 
************************************/



abstract class Member {
	public $firstName;
	public $lastName;
	public $biography;
	public $website;
	protected $member_id;

	//Concrete functionality for all children below here:
	public function __construct($firstName, $lastName, $bio, $webpage, $id){
		$this->firstName = $firstName;
		$this->lastName = $lastName;
		$this->biography = $bio;
		$this->website = $webpage;
		//The id comes from the database
		$this->member_id = $id;
	}
	public function getFullName(){		return $this->firstName . ' ' . $this->lastName;}
	public function getFirstName(){		return $this->firstName; 	}
	public function getLastName(){ 		return $this->lastName;     }
	public function getBIO(){			return $this->biography;	}
	public function getWebsite(){		return $this->website;		}
	public function getID(){			return $this->member_id;	}

}

class R332_Member extends Member{
	//An R332 Member has help hours! Stored in an associative array! with days of the week as the key?
	public $hours;

	public function __construct($firstName,$lastName, $bio, $webpage, $id, $hoursArray){
		//Expecting an array in addition to normal things. an array full of help_hour objects
		parent::__construct($firstName,$lastName, $bio, $webpage, $id);
		$this->hours = $hoursArray;
	}	
}

class Forum_Member extends Member{
	//A forum member has a password and probably more.
	private $password;

}











?>