<?php

/**********************************/
/* Subclass of SQL_Connector 
/* Ethan Eldridge.  June 1st 2012
/* Subclass of SQL connector designed to
/* provide specific functionality to the
/* news page
/* We make the assumption of running on
************************************/

//We need our config file for database information
//and of course we need our parent to inherit from
require_once('../Configuration/config.php');
require_once('../Models/SqlConnector.php');


class NewsPageDB extends Sql_Connector{
	//This is a singleton class because we only really want one connection going on
	private static $singleInstance;


	//The constructor is inherited from the SQL_Connector
	public function __destruct(){
		//Destructor to ensure disconnection from database
		parent::__destruct();
	}

	public static function getInstance(){
		//Creates an instance if none exists, otherwise it returns the singleton
		if (!self::$singleInstance)
		{
			self::$singleInstance = new hoursPageDB();
		}
		return self::$singleInstance;
	}

	public function connect_to_db(){
		//Connect to the database and select the database			
		try{
			$this->connection = new PDO("mysql:host=$this->host;dbname=$this->dbName", $this->user, $this->dbPass);
			$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}catch(PDOException $err){
			die('Could not connect to database ' . $err->getMessage());
		}		
	}

	public function makeStory(NewsStory $story){
		//Function should take an instance of a story and
		//then generate the story entry to the database. 

		//We have to be connected for anything to work
		if(!isset($this->connection)){
			die('You must set a database connection before calling functions that use the database!');
			//Instead of the above connect to database?
		}
		//XXX SHOULD THINK ABOUT DOING THIS: we check for connect, if there isn't one then we make one?
		//Gotta make a little bit of data for the query. 
		$today = date("N Y-m-d");
		$toID = 0; //This should become something defined by our forum stuff XXX
		
		//Construct the insertion query (any validation on content (for malicous intent should be done before here))
		$statement = $this->connection->prepare( "INSERT INTO News (author, date_posted, picture_url, picture_caption, story_url, title, toID) VALUES (?,?,?,?,?,?,?);");

		//Add in the data to be sent.
		$statement->bindValue(1,$story->getAuthor(), PDO::PARAM_STR);
		$statement->bindValue(2,$today, $PDO::PARAM_STR);
		$statement->bindValue(3,$story->getPictureURL(), PDO::PARAM_STR);
		$statement->bindValue(4,$story->GetCaption(), PDO::PARAM_STR);
		$statement->bindValue(5,$story->getContent(), PDO::PARAM_STR);
		$statement->bindValue(6,$story->getTitle(), PDO::PARAM_STR);
		$statement->bindValue(7,$toID, PDO::PARAM_INT);

		//Run the insertion and return OK
		$statement->execute();
		return ENTRY_OKAY;
	}

	public function getStories(){
		//Returns an array of NewsStory objects ordered by date, newest first.
		die('not implemented?');
	}

}









?>