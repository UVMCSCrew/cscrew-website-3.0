<?php
/**********************************/
/* Ethan J. Eldridge.  June 1st 2012
/* 
/* Contains the definition for the class Story
/* I don't think this will have any children,
/* but later on when implementing forums it might
/* end up using a 'postable' interface so we can post
/* it on the forums and comment on it and stuff.
************************************/

//Thinking about it, we could extend entry here, userID would become the ID
//Then we could use the injection validator on the content... hmmm meh I 
//think we'll be fine because stories are made by us and us alone.
//And we're using a content_url, and we can make content its own class if we
//want and make a validator for it or something
class NewsStory{
	private $ID;
	public $author;
	public $date_posted;
	public $picture_url;
	public $caption;
	public $title;
	public $content_url;

	public function __construct($ID,$author,$title,$postDate,$picURL, $picCaption, $contents_path){
		$this->ID = $ID;
		$this->author = $author;
		$this->date_posted = $postDate;
		$this->picture_url = $picURL;
		$this->caption = $picCaption;
		$this->title = $title;
		$this->contents_url = $contents_path;
	}

	public function getID(){ 		return $this->ID;    	   }
	public function getAuthor(){ 	return $this->author;      }
	public function getPostDate(){	return $this->date_posted; }
	public function getPictureURL(){return $this->picture_url; }
	public function getCaption(){	return $this->caption;     }
	public function getTitle(){		return $this->title;	   }
	public function getContent(){	return $this->contents_url;}
}
