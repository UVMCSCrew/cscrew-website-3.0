<?php

/**********************************/
/* Ethan Eldridge
/* July 1st 2012
/* Hours entity to hold onto
/* members hours so we don't have
/* an array for start times, ends times, etc
************************************/

//We need our config file for database information
//and of course we need our parent to inherit from

//require_once('../Prototyping/Member.php');

//Thinking about maybe having this extend postable somehow
class HelpHour{
	public $day;
	public $start;
	public $end;

	public function __construct($day, $start_time, $end_time){
		//This function expects days as: M T W R F S Su and 
		//the times to be in HH:MM:SS format for both end/start
		$this->day = $day;
		$this->start = $start_time;
		$this->end = $end_time;
	}
}
	
	
?>