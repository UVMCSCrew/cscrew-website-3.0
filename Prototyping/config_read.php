<?php 
/**************************
/*Ethan Eldridge
/*May 20th 2012
/*This file includes the following:
/*1) config_read function:
/*   Reads the configuration file and returns an associative array with
/*   with each value stored away. Check the config file for keys and to
/*   to change the values. ../Configuration/config.txt should be its location
/*
/*   Each value from the associative array can be accessed via the following keys:
/*   Semester, ClassCount, Year,DB_Name,DB_Pass,DB_User,DB_Host
**************************/

function config_read($filepath_to_config){
	//Does the file even exist? Validate the filepath
	if (!file_exists($filepath_to_config)){
			die('Can\'t locate the configuration file! Error');
	}
	//If the config file exists we can snag what we'd like out of it. 
	//It's a small file so we'll load it all and regEX it
	//need nl2br to include new lines for us so we can regEX properly
	$conf_file = nl2br(file_get_contents($filepath_to_config));


	//If we add more stuff to the config file I'll have to change this
	//but its ok because a single change is much better. Yay modular code

	preg_match("/Spring|Fall/",$conf_file,$Semester);
	preg_match_all("/\d+/", $conf_file,$ClassCount_and_Year);
	//The database stuff requires two steps
	preg_match("/DB_Name = [A-Za-z.]+/", $conf_file,$DB_Name);
	$DB_Name = preg_split("/=/", $DB_Name[0]);
	preg_match("/DB_Host = [A-Za-z.]+/", $conf_file,$DB_Host);
	$DB_Host = preg_split("/=/", $DB_Host[0]);
	preg_match("/DB_User = [A-Za-z.]+/", $conf_file,$DB_User);
	$DB_User = preg_split("/=/", $DB_User[0]);
	preg_match("/DB_Pass = [A-Za-z.]+/", $conf_file, $DB_Pass);
	$DB_Pass = preg_split("/=/", $DB_Pass[0]);
	

	//Reassign so we're not making multi-dimensional arrays
	$Semester = $Semester[0];
	$ClassCount = $ClassCount_and_Year[0][0];
	$Year = $ClassCount_and_Year[0][1];
	$DB_Name = $DB_Name[1];
	$DB_Host = $DB_Host[1];
	$DB_User = $DB_User[1];
	$DB_Pass = $DB_Pass[1];

	//Store all the stuff into an array to return
	$returning_array = array(
			"Semester" => $Semester,
			"ClassCount" => $ClassCount,
			"Year" => $Year,
			"DB_Name" => $DB_Name,
			"DB_Pass" => $DB_Pass,
			"DB_User" => $DB_User,
			"DB_Host" => $DB_Host,
	);


	return $returning_array;
}