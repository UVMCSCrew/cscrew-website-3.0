<?php

/**********************************/
/* Subclass of SQL_Connector 
/* Ethan Eldridge.  May 30th 2012
/* Subclass of SQL connector designed to
/* provide specific functionality to the
/* hours page
/* We make the assumption of running on
/* our database where we have mysql installed
************************************/

//We need our config file for database information
//and of course we need our parent to inherit from
require_once('../Configuration/config.php');
require_once('../Models/SqlConnector.php');
require_once('../Prototyping/Member.php');
require_once('../Prototyping/HelpHour.php');


class HoursPageDB extends Sql_Connector{
	//This is a singleton class because we only really want one connection going on
	private static $singleInstance;


	//The constructor is inherited from the SQL_Connector
	public function __destruct(){
		//Destructor to ensure disconnection from database
		parent::__destruct();
	}

	public static function getInstance(){
		//Creates an instance if none exists, otherwise it returns the singleton
		if (!self::$singleInstance)
		{
			self::$singleInstance = new hoursPageDB();
		}
		return self::$singleInstance;
	}

	public function connect_to_db(){
		//Connect to the database and select the database			
		try{
			$this->connection = new PDO("mysql:host=$this->host;dbname=$this->dbName", $this->user, $this->dbPass);
			$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}catch(PDOException $err){
			die('Could not connect to database ' . $err->getMessage());
		}		
	}

	public function getKnownLanguages(){
		//Returns an array of string's with all the languages known by the CS Crew 
		//The known languages are also listed in knownLanguages.txt and running an
		//admin script ( updateDB_lang.rb) will push that txt file to the database
		if(!isset($this->connection)){
			die('You must set a database connection before calling functions that use the database!');
		}
		//Prepare and run the query for languages:
		$statement = $this->connection->prepare("SELECT Language FROM R332_Languages ORDER BY pkLanguageID");
		$statement->execute();

		//Throw the results into an array to be returned
		$knownLanguages = $statement->fetchAll();
		$languageArray = array();
		foreach ($knownLanguages as $language) {          
			$languageArray[] = $language;                 
		}

		//I'm expecting this function to be used to echo out the languages into a list
		//but for proper modularity, we'll leave it to some controller to render the
		//list properly. Also, because we've ordered them by pkLanguageID, the language ID
		//is the placement in the array of the language string itself(+1). So if Java is first, then it's ID is 1.
		return $languageArray;
	}

	public function getLanguageExperts($desiredLanguage){
		//this function returns the ID's of the members who know the 
		//desired languages, this does not return a member object because
		//that might slow everything down/
		if(!isset($this->connection)){
			die('You must set a database connection before calling functions that use the database!');
		}

		//Figure out what the ID of the language is:
		$lan_statement = $this->connection->prepare("SELECT pkLanguageID FROM R332_Languages WHERE Language = '$desiredLanguage';");
		$lan_statement->execute();
		$languageID = $lan_statement->fetch();

		var_dump($languageID);

		//Use the ID to find out which members know the language
		$mem_statement = $this->connection->prepare("SELECT fkMemberID FROM R332_MemberLanguage WHERE fkLanguageID = '$languageID';");
		$mem_statement->execute();

		//Place the results into an array to be returned
		$experts = $mem_statement->fetchAll();
		$returnArray = array();
		foreach($experts as $expert){
			$returnArray[] = $expert;
		}

		return $returnArray;
	}

	public function getMemberByID($mem_ID){
		//Self-documenting code see the above line.
		//This function will return a R332_Member Object

		if(!isset($this->connection)){
			die('We need a connection check out connect_to_db function');
		}

		//Grab the member by their ID (should only return one result if any)
		$mem_statement = $this->connection->prepare("SELECT * FROM R332_Members WHERE pkMemberID = '$mem_ID';");
		$mem_statement->execute();
		$db_info = $mem_statement->fetch();

		//Grab their hours and turn it into an array
		//something like "SELECT * FROM R332_Hours WHERE fkMemberID = '$mem_ID';"
		$hours_getter = $this->connection->prepare("SELECT * FROM R332_Hours WHERE fkMemberID = '$mem_ID';");
		$hours_getter->execute();
		$mem_hoursArray = array();
		$sql_Results = $hours_getter->fetchAll();
		//loop through the days of the week we grabbed and add them to the array
		foreach($sql_Results as $result){
			//This is rather dependent on our db structure so if anyone renames anything this must change as well
			$mem_hoursArray[]= new HelpHour($result['DayOfWeek'],$result['StartTime'],$result['EndTime']);
		}

		//Create a member object and return it
		$member = new R332_Member($db_info['FirstName'],$db_info['LastName'], $db_info['Bio'], $db_info['Website'], $mem_ID, $mem_hoursArray);
		return $member;
	}

}

$test = HoursPageDB::getInstance();
$test->connect_to_db();
var_dump($test->getMemberByID('pkiripol'));









?>