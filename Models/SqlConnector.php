<?php
/**********************************/
/* mySQL Connecting Class
/* Ethan Eldridge.  May 19th 2012
/* Gonna make a basic class to control
/* the connections to the database 
/* because it seems like a good idea.
/* We make the assumption of running on
/* our database where we have mysql installed
************************************/

//We require some database access, which are conventiantly stored in a configuration file
//much better than reading and parsing a file for the options, even if doing so provides
//a non-programmer to changes things easily, since this site will be maintened hopefully
//by semi competetant people when we're done I think it's aright.
require_once('Configuration/config.php');

abstract class SQL_Connector {
	//Basic Variables to be defined by configuration files
	protected $user;
	protected $host;
	protected $dbName;
	protected $dbPass;

	//Some constants to be used by children for general status's
	const ENTRY_OKAY = 0;
	const CANT_CONNECT_TO_DB = 1;
	const NOT_CONNECTED_TO_DB = 5;

	//To be defined when the file connects to mysql
	protected $connection;

	//This is a singleton class for speed and memory
	private static $singleInstance;

	//The constructor is protected for subclassing and creating singletons of the subclasses, final because
	//we must have the configutarion file stuff.
	protected final function __construct() {
		//Get the configuration options from config.php (stored in the configuration folder -suprise-)
		//And set the fields based on the variables defined there
		$this->user = DATABASE_USER;
		$this->host = DATABASE_HOST;
		$this->dbName = DATABASE_NAME;
		$this->dbPass = DATABASE_PASS;
		
	}

	public function __destruct(){
		//Destructor to ensure disconnection from database
		if($this->connection){
			$this->close_connection_to_db();
		}
	}

	//abstract to force singletons
	abstract public static function getInstance();

	//leave it up to implementation to connect
	abstract public function connect_to_db();

	//we'll close things for them though
	public function close_connection_to_db(){
		//Closes the connection to the database... self doc code what?
		if(isset($this->connection)){
			//This is apparently the way to enforce closing a database for PDO, I personally
			//prefer having an explicit mysql_close() but that's not possibly because of PDO
			$this->connection = null;
		}
	}
	//This function allows us to submit our own custom queries, while it doesn't prepare
	//or anything safe, it's for when we really need something and don't want to write a 
	//function to do it. Also, child classes can use this instead of the longer PDO statements
	public function query($message){
		if(!isset($this->connection)){
			return self::NOT_CONNECTED_TO_DB;
		}
		$query = $this->connection->exec($message);
		return $query;
	}

}
// //Some test code
// echo "making class\n </br>";
// $testd = SQL_Connector::getInstance();
// echo "attempting to connect\n </br>";
// $dbc = $testd->connect_to_db();
// $query = "SELECT pkPersonID, FirstName, LastName, StartTime, EndTime
// 			FROM R332_Person
// 			Inner Join R332_Hours on pkPersonID = fkPersonID
// 			WHERE DayOfWeek= '". $shortDay ."' AND Semester='" . $semester . "' AND Active =1 
// 			ORDER BY StartTime ASC";	
// $result = $testd->query($query);	
// echo $result;
// $testd->close_connection_to_db();
?>