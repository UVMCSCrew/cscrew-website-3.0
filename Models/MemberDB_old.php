<?php
/**********************************/
/* Subclass of SQL_Connector 
/* Ethan Eldridge.  May 23rd 2012
/* Subclass of SQL connector designed to
/* provide specific functionality to the
/* login page for members and forums
/* We make the assumption of running on
/* our database where we have mysql installed
************************************/

require_once('Configuration/config.php');
require_once('Models/SqlConnector.php');

//To enforce our object oriented approach for this:
require_once('Models/Entry.php');
require_once('Models/EntryValidation.php');


class MemberDB extends SQL_Connector {
	//This is a singleton class for speed and memory
	private static $singleInstance;

	//Some status codes:
	const INVALID_NET_ID = 2;
	const INVALID_CONTENT= 3;
	const INVALID_PASSWORD=5;

	//The constructor is inherited from the SQL_Connector
	public function __destruct(){
		//Destructor to ensure disconnection from database
		parent::__destruct();
	}

	public static function getInstance(){
		//Creates an instance if none exists, otherwise it returns the singleton
		if (!self::$singleInstance)
		{
			self::$singleInstance = new MemberDB();
		}
		return self::$singleInstance;
	}

	public function connect_to_db(){
		//Connects to the data base using the info stored in configuration files
		//Fun Fact: @ suppresses errors of the expression it prepends
		
		//Connect to the database and select the database		
		try{
			$this->connection = new PDO("mysql:host=$this->host;dbname=$this->dbName", $this->user, $this->dbPass);
			$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return self::ENTRY_OKAY;

		}catch(PDOException $err){
			die('Could not connect to database ' . $err->getMessage());
			//I'm aware that this return is never called. I'm thinking of removing the die statements
			//and expecting the calling code to be able to handle the status codes instead
			return self::CANT_CONNECT_TO_DB;
		}		
	}

	private function saltPassword($password){

	}

	public function userExists($username){
		//expecting a string, and returns true or false if we can find the user in the account info area
		//selecting priv level because its small, and as long as we don't get an empty result the name exists
		$uCheck = $this->connection->prepare("SELECT priv_level FROM Account_Info WHERE user_id = ?;");
		$uCheck->bindValue(1,$username,PDO::PARAM_STR);
		$uCheck->execute();

		if($uCheck->rowCount() > 0){
			return true;
		}

		return false;



	}

	//CAn't be static function because we need a connection the db
	public function createEntryFromUserPass($User, $Password){
		//I plan to change this so that it takes an instance of some type of data collection
		//object, but maybe simple strings would be better. Or taking a POST array.
		//I would prefer that we have an instance of some type that I could validate on for malicious intent
		//so a simple entry object would work best, using the content as the password or something
		if(!isset($this->connection)){
			die('You must set a database connection before calling functions that use the database!!');
		}

		if(checkPassword($Password)){
			//return an entry object
		}else{
			//no. 
		}

	}

	public function checkPassword($entry){
		//Checks that the ID matches the password used returns true if
		//password is correct, false if not		 
		$passCheck = $this->connection->prepare("SELECT password FROM Account_Info WHERE user_id= ?;");
		//password is correct, false if not
		$passCheck = $this->connection->prepare("SELECT password FROM Account_Info WHERE user_id = ?;");
		$passCheck->bindValue(1, $entry->getUserID(), PDO::PARAM_STR);
		$passCheck->execute();

		//grab the password
		$password = $passCheck->fetch();

		//compare it to the entries password
		if(strcmp($password['password'], $entry->getPassword())==0 ){
			return true;
		}
		return false;
	}

	
	public function logUsage(WebsiteLogin $entry){
		///Make sure we're connected!
		if(!isset($this->connection)){
			die('You must set a database connection before calling functions that use the database!');
		}
		
		//This function will log a login to the database, so we should authenticate and somewhere around here
		$ID_validator = new UVMValidator($entry);
		$content_validator = new InjectionValidator($entry);

		//Now we begin the checks if anything is wrong we'll return the appropriate status code
		if(!$ID_validator->validate()){
			return self::INVALID_NET_ID;
		}

		if(!$content_validator->validate()){
			return self::INVALID_CONTENT;
		}


		if(!$this->checkPassword($entry)){
			return self::INVALID_PASSWORD;
		}

		//Remove sensitive data from the entry object
		$entry->clearPassword();

		$statement = $this->connection->prepare( "INSERT INTO Member_Logins (fkUserID, IP ) VALUES ( ?, ?);");
		
		//Bind the values to the query
		$statement->bindValue(1,$entry->getUserID(),     PDO::PARAM_STR);
		//Grab the IP Address of the client
		$statement->bindValue(2,$_SERVER['REMOTE_ADDR'], PDO::PARAM_STR);
		//execute the insertion
		$statement->execute();

		//set the privelege level from the entry we'll get from the database
		//XXXX JOSH! Would it be ok to set a privilege level here?
		$privilegeSet = $this->connection->prepare("SELECT priv_level FROM Account_Info WHERE user_id = ?;");
		$privilegeSet->bindValue(1,$entry->getUserID(), PDO::PARAM_STR);
		$privilegeSet->execute();

		//Store the result in the session
		$result = $privilegeSet->fetch();
		$_SESSION['priv'] = $result['priv_level'];
		//Just in case of mismatch we'll fix that
		$entry->priv_level = $result['priv_level'];

		//ENTRY OKAY inherited from parent class
		return self::ENTRY_OKAY;
	}
}
// //Some test code
//  echo "making MemberDB\n </br>";
// $testd = MemberDB::getInstance();
// echo "attempting to connect\n </br>";
// $dbc = $testd->connect_to_db();


 
// $testarray =  array('netId' => "ejeldrid", 'password' => "password", 'priv_level' => 3);
// $entry = new WebsiteLogin("",$testarray );
// $testd->logUsage($entry);
// $testd->close_connection_to_db();

?>