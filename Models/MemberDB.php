<?php
/**********************************/
/* Subclass of SQL_Connector 
/* Ethan Eldridge.  May 23rd 2012
/* Subclass of SQL connector designed to
/* provide specific functionality to the
/* login page for members and forums
/* We make the assumption of running on
/* our database where we have mysql installed
************************************/

require_once('Configuration/config.php');
require_once('Models/SqlConnector.php');

//To enforce our object oriented approach for this:
require_once('Models/Entry.php');
require_once('Models/EntryValidation.php');
require_once('Models/User.php');


class MemberDB extends SQL_Connector {
	//This is a singleton class for speed and memory
	private static $singleInstance;

	//Some status codes:
	const INVALID_NET_ID = 2;
	const INVALID_CONTENT= 3;
	const INVALID_PASSWORD=5;

	//The constructor is inherited from the SQL_Connector
	public function __destruct(){
		//Destructor to ensure disconnection from database
		parent::__destruct();
	}

	public static function getInstance(){
		//Creates an instance if none exists, otherwise it returns the singleton
		if (!self::$singleInstance)
		{
			self::$singleInstance = new MemberDB();
		}
		return self::$singleInstance;
	}

	public function connect_to_db(){
		//Connects to the data base using the info stored in configuration files
		//Fun Fact: @ suppresses errors of the expression it prepends
		
		//Connect to the database and select the database		
		try{
			$this->connection = new PDO("mysql:host=$this->host;dbname=$this->dbName", $this->user, $this->dbPass);
			$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return self::ENTRY_OKAY;

		}catch(PDOException $err){
			die('Could not connect to database ' . $err->getMessage());
			//I'm aware that this return is never called. I'm thinking of removing the die statements
			//and expecting the calling code to be able to handle the status codes instead
			return self::CANT_CONNECT_TO_DB;
		}		
	}


	public function userExists($username){
		//expecting a string, and returns true or false if we can find the user in the account info area
		//selecting priv level because its small, and as long as we don't get an empty result the name exists
		$uCheck = $this->connection->prepare("SELECT priv_level FROM Account_Info WHERE user_id = ?;");
		$uCheck->bindValue(1,$username,PDO::PARAM_STR);
		$uCheck->execute();

		if($uCheck->rowCount() > 0){
			return true;
		}

		return false;



	}

	//CAn't be static function because we need a connection the db
	public function createEntryFromUserPass($User, $Password){
		//I plan to change this so that it takes an instance of some type of data collection
		//object, but maybe simple strings would be better. Or taking a POST array.
		//I would prefer that we have an instance of some type that I could validate on for malicious intent
		//so a simple entry object would work best, using the content as the password or something
		if(!isset($this->connection)){
			die('You must set a database connection before calling functions that use the database!!');
		}

		if(checkPassword($Password)){
			//return an entry object
		}else{
			//no. 
		}

	}

	public function checkPassword($entry){
		//Checks that the ID matches the password used returns true if
		//password is correct, false if not		 
		$passCheck = $this->connection->prepare("SELECT password FROM Account_Info WHERE user_id= ?;");
		//password is correct, false if not
		$passCheck = $this->connection->prepare("SELECT password FROM Account_Info WHERE user_id = ?;");
		$passCheck->bindValue(1, $entry->getUserID(), PDO::PARAM_STR);
		$passCheck->execute();

		//grab the password
		$password = $passCheck->fetch();

		//compare it to the entries password
		if(strcmp($password['password'], $entry->getPassword())==0 ){
			return true;
		}
		return false;
	}

	public function signUpNewUser(NewUser $nUser){
		//Validate the object!
		if(!isset($this->connection)){
			die('You must be connected to the database!');
		}
		//Check for injection here and duplicants:
		// package new user data into an array so Entry 
		// can take it as $POST
		$newUsrPkg = array("uvm_id" => $nUser->uvm_id);
		$invalObj = new Entry("dummy", $newUsrPkg);
		$validation = new DuplicateIDValidator($invalObj);
		if($validation->validate()){
			return self::INVALID_NET_ID;
		}

		//Add in the object if its valid
		$insertNUser = $this->connection->prepare("INSERT INTO Account_Info (user_id,username,password,priv_level,email) VALUES ( ?,?,?,?,?); ");
		$insertNUser->bindValue(1, $nUser->uvm_id, PDO::PARAM_STR);
		$insertNUser->bindValue(2, $nUser->username, PDO::PARAM_STR);
		$insertNUser->bindValue(3, $nUser->password, PDO::PARAM_STR);
		$insertNUser->bindValue(4, 1, PDO::PARAM_INT);
		$insertNUser->bindValue(5, $nUser->uvm_id . '@uvm.edu', PDO::PARAM_STR);
		$insertNUser->execute();

		$insertNUser = $this->connection->prepare("INSERT INTO Account_profile (uvm_id) VALUES (?); ");
		$insertNUser->bindValue(1, $nUser->uvm_id, PDO::PARAM_STR);
		$insertNUser->execute();

		$insertNUser = $this->connection->prepare("INSERT INTO Social_Networks (uvm_id) VALUES (?); ");
		$insertNUser->bindValue(1, $nUser->uvm_id, PDO::PARAM_STR);
		$insertNUser->execute();


		return self::ENTRY_OKAY;
	}

	public function editProfile(UserModel $User){
		//Validate the object!
		if(!isset($this->connection)){
			die('You must be connected to the database!');
		}
		//Add in the object if its valid
		// here we use the fillWithNull() function to insert null entries if an
		// form input is left blank
		$insertNUser = $this->connection->prepare("UPDATE Account_profile SET username=?,first_name=?,last_name=?,personal_url=?, about_me=?, img_url=? WHERE uvm_id =?; ");
		$insertNUser->bindValue(1, $this->fillWithNull($User->username), PDO::PARAM_STR);
		$insertNUser->bindValue(2, $this->fillWithNull($User->first_name), PDO::PARAM_STR);
		$insertNUser->bindValue(3, $this->fillWithNull($User->last_name), PDO::PARAM_STR);
		$insertNUser->bindValue(4, $this->fillWithNull($User->personal_url), PDO::PARAM_STR);
		$insertNUser->bindValue(5, $this->fillWithNull($User->about_me), PDO::PARAM_STR);
		$insertNUser->bindValue(6, $this->fillWithNull($User->img_url), PDO::PARAM_STR);
		$insertNUser->bindValue(7, $_SESSION['uvm_id'], PDO::PARAM_STR);
		$insertNUser->execute();

		// var_dump($User->social);

		// update Social_Networks tables
		$insertSocial = $this->connection->prepare("UPDATE Social_Networks SET twitter=?, facebook=?,tumblr=?,blogger=?, google=?, rss=?, pintrest=?, reddit=?, myspace=?, git=?, linkedin=? WHERE uvm_id =?; ");
		$insertSocial->bindValue(1, $this->fillWithNull($User->social['twitter']), PDO::PARAM_STR);
		$insertSocial->bindValue(2, $this->fillWithNull($User->social['facebook']), PDO::PARAM_STR);
		$insertSocial->bindValue(3, $this->fillWithNull($User->social['tumblr']), PDO::PARAM_STR);
		$insertSocial->bindValue(4, $this->fillWithNull($User->social['blogger']), PDO::PARAM_STR);
		$insertSocial->bindValue(5, $this->fillWithNull($User->social['google']), PDO::PARAM_STR);
		$insertSocial->bindValue(6, $this->fillWithNull($User->social['rss']), PDO::PARAM_STR);
		$insertSocial->bindValue(7, $this->fillWithNull($User->social['pintrest']), PDO::PARAM_STR);
		$insertSocial->bindValue(8, $this->fillWithNull($User->social['reddit']), PDO::PARAM_STR);
		$insertSocial->bindValue(9, $this->fillWithNull($User->social['myspace']), PDO::PARAM_STR);
		$insertSocial->bindValue(10, $this->fillWithNull($User->social['git']), PDO::PARAM_STR);
		$insertSocial->bindValue(11, $this->fillWithNull($User->social['linkedin']), PDO::PARAM_STR);
		$insertSocial->bindValue(12, $_SESSION['uvm_id'], PDO::PARAM_STR);
		$insertSocial->execute();

		return self::ENTRY_OKAY;
	} // end editProfile

	public function getProfileData(){
		if(!isset($this->connection)){
			die('You must be connected to the database!');
		}

		// fetch the user's personal info from the Account_profle table
		$getProfile = $this->connection->prepare("SELECT * FROM Account_profile WHERE uvm_id =?; ");
		$getProfile->bindValue(1, $_SESSION['uvm_id'], PDO::PARAM_STR);
		$getProfile->execute();
		$result1 = $getProfile->fetch(PDO::FETCH_BOTH);

		// fetch the user's social networks from the Social_Networks table
		$getProfile = $this->connection->prepare("SELECT * FROM Social_Networks WHERE uvm_id =?; ");
		$getProfile->bindValue(1, $_SESSION['uvm_id'], PDO::PARAM_STR);
		$getProfile->execute();
		$result2 = $getProfile->fetch(PDO::FETCH_BOTH);

		// merge the results into a single array (for $vars[])
		$result = array_merge($result1, $result2);
		return $result;

	} // end getProfileData


	public function logUsage(WebsiteLogin $entry){
		///Make sure we're connected!
		if(!isset($this->connection)){
			die('You must set a database connection before calling functions that use the database!');
		}
		
		//This function will log a login to the database, so we should authenticate and somewhere around here
		$ID_validator = new UVMValidator($entry);

		//Now we begin the checks if anything is wrong we'll return the appropriate status code
		if(!$ID_validator->validate()){
			return self::INVALID_NET_ID;
		}


		// if(!$this->checkPassword($entry)){
		// 	return self::INVALID_PASSWORD;
		// }

		//Remove sensitive data from the entry object
		$entry->clearPassword();

		$statement = $this->connection->prepare( "INSERT INTO Member_Logins (fkUserID, IP ) VALUES ( ?, ?);");
		
		//Bind the values to the query
		$statement->bindValue(1,$entry->getUserID(),     PDO::PARAM_STR);
		//Grab the IP Address of the client
		$statement->bindValue(2,$_SERVER['REMOTE_ADDR'], PDO::PARAM_STR);
		//execute the insertion
		$statement->execute();

		//set the privelege level from the entry we'll get from the database
		//XXXX JOSH! Would it be ok to set a privilege level here?
		$privilegeSet = $this->connection->prepare("SELECT priv_level FROM Account_Info WHERE user_id = ?;");
		$privilegeSet->bindValue(1,$entry->getUserID(), PDO::PARAM_STR);
		$privilegeSet->execute();

		//Store the result in the session
		$result = $privilegeSet->fetch();
		$_SESSION['priv'] = $result['priv_level'];
		//Just in case of mismatch we'll fix that
		$entry->priv_level = $result['priv_level'];

		//ENTRY OKAY inherited from parent class
		return self::ENTRY_OKAY;
	}

	public function fillWithNull($testVar){
		// this function is used when form values are left blank (on optional inputs)
		// and instead of no value (which would throw an error on our INSERT statements) values
		// stored as null
		if ($testVar != "null"){
			return $testVar;
		}
		else{
			return "null";
		}
	}
}
// //Some test code
//  echo "making MemberDB\n </br>";
// $testd = MemberDB::getInstance();
// echo "attempting to connect\n </br>";
// $dbc = $testd->connect_to_db();


 
// $testarray =  array('netId' => "ejeldrid", 'password' => "password", 'priv_level' => 3);
// $entry = new WebsiteLogin("",$testarray );
// $testd->logUsage($entry);
// $testd->close_connection_to_db();

?>