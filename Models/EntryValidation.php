 	<?php
/***********************/
/*Ethan J. Eldridge
/*May 27th 2012
/*Pfft, its just 5:02 in the morning no worries here
/*This file contains the Entry Validation classes
***********************/

//We need to be able to reference Entry classes
require_once('Models/Entry.php');
//included for duplicant checker
require_once('Models/MemberDB.php');


//Simple interface for common calls for validators
abstract class ValidateEntry{
	protected $entry;
	public final function __construct(Entry $entry){
		$this->entry = clone $entry;
	}
	//This function will return true or false, afterall it is a validator
	abstract function validate();
}

//Duplicant ID checker for the database to work with
class DuplicateIDValidator extends ValidateEntry{
	function validate(){
		//Checks the database to see if the user_id is duplicated already
		//is it a non malicious id? can never be sure
		$securitycheck = new UVMValidator($this->entry);
		if(!$securitycheck->validate()){
			//not a valid uvm id!!!! This also forces new users who user this function to be uvm students
			return false;
		}
		//this is the only validator that really depends on an outside file besides entry.
		$dbconnection = MemberDB::getInstance();
		if($dbconnection->connect_to_db() == MemberDB::ENTRY_OKAY){
			//connected do our thing
			return $dbconnection->userExists($this->entry->getUserID());
		}else{
			//can't connect so who knows, lets just not.
			return false;
		}
	}

}


//And we need one for UVM which will be a bit more complicated than the above
class UVMValidator extends ValidateEntry{
	function validate(){
		//This function is a little bit crazy because we have to run off the the LDAP
		//server and verify that the netID we've been passed is a valid UVM net ID.
		// if(is_array(UVMValidator::search_LDAP_for_user($this->entry->getUserID()))){
		// 	//Enough parenthesis up there to make Snapp grin with lisp-y pleasure
		// 	return true;
		// }
		// return false;
		return true;
	}

	//This function is made static so we can use the search outside of this class
	///not the best object oriented practice, but at least it's in an inteligent
	//and appropriate class for it. Function returns an array or false depending on search
	//XXXX  Side note for later: Member class should implement a getUVMUserInfo from the $info returned from this XXXXX//
	public static function search_LDAP_for_user($uvmNetID){
		//This code basically speaks for itself but, it's important to note
		//that if the ldap server ever goes down we're probably screwed :p
		//Courtesy goes to Beau, Mark, or Tony for coming up with this originally
		//(My bet's on Beau)
	// 	$server_connection = ldap_connect("ldap.uvm.edu");

	// 	//Hopefully we connected
	// 	if($server_connection){
	// 		//Bind to the server
	// 		$shackles = ldap_bind($server_connection);

	// 		//Specify where in the directory we'd like to start
	// 		$distinguished_name = "uid=$uvmNetID,ou=People,dc=uvm,dc=edu";
	// 		$filter = "( | (netid=$uvmNetID ) )";

	// 		//what do we want?
	// 		$findThese = array( "sn", "givenname", "mail", "ou" );

	// 		//When do we want it? Now!
	// 		if( $searchResults = @ldap_search($server_connection, $distinguished_name, $filter, $findThese)){
	// 			//If we have more than 0 results:
	// 			if( ldap_count_entries($server_connection, $searchResults ) > 0 ){	
	// 				$info = ldap_get_entries($server_connection, $searchResults);
	// 				ldap_close($server_connection);
	// 				return $info;
	// 			}else{
	// 				return false;
	// 			}
	// 		}
	// 	}
	// 	//It's my understanding that this will un-bind our schackles from earlier
	// 	ldap_close($server_connection);
	// 	return false;
	 }
}
//Some test code:
// $testEntryV = new RoomLoginEntry("ejeldrid", 'derp', 'adventuring', 'CS 000');
// $testEntryNV = new RoomLoginEntry('notastudent','herp','prupose','CS 001');
// $valid1 = new UVMValidator($testEntryV);
// $valid2 = new UVMValidator($testEntryNV);
// var_dump( $valid1->validate());
// var_dump($valid2->validate());
?>


