<?php
/**********************************/
/* Subclass of SQL_Connector 
/* Ethan Eldridge.  May 23rd 2012
/* Subclass of SQL connector designed to
/* provide specific functionality to the
/* login page for room 332
/* We make the assumption of running on
/* our database where we have mysql installed
************************************/

//We require some database access, which are conventiantly stored in a configuration file
//much better than reading and parsing a file for the options, even if doing so provides
//a non-programmer to changes things easily, since this site will be maintened hopefully
//by semi competetant people when we're done I think it's aright.
require_once('Models/Entry.php');
require_once('Models/SqlConnector.php');


class RoomSignInDB extends SQL_Connector {
	//This is a singleton class for speed and memory
	private static $singleInstance;

	//Some status codes:
	const INVALID_NET_ID = 2;
	const INVALID_CONTENT= 3;

	//The constructor is inherited from the SQL_Connector
	public function __destruct(){
		//Destructor to ensure disconnection from database
		parent::__destruct();
	}

	public static function getInstance(){
		//Creates an instance if none exists, otherwise it returns the singleton
		if (!self::$singleInstance)
		{
			self::$singleInstance = new RoomSignInDB();
		}
		return self::$singleInstance;
	}

	public function connect_to_db(){
		//Connects to the data base using the info stored in configuration files
		//Fun Fact: @ suppresses errors of the expression it prepends
		
		//Connect to the database and select the database			
		try{
			$this->connection = new PDO("mysql:host=$this->host;dbname=$this->dbName", $this->user, $this->dbPass);
			$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return ENTRY_OKAY;
		}catch(PDOException $err){
			die('Could not connect to database ' . $err->getMessage());
			//I'm aware that this return is never called. I'm thinking of removing the die statements
			//and expecting the calling code to be able to handle the status codes instead
			return self::CANT_CONNECT_TO_DB;
		}		
	}

	public function getClassList(){
		//make sure we have a connection first!
		if(!isset($this->connection)){
			die('You must set a database connection before calling functions that use the database!');
			//See line 57...
			return self::NOT_CONNECTED_TO_DB;
		}
		//Prepare the query!
		$statement = $this->connection->prepare("SELECT * FROM login_class WHERE Semester = ? ORDER BY pkNumber;" );
		//The $SEMESTER variable comes from config.php

		$statement->bindValue(1, SEMESTER, PDO::PARAM_STR);
		//Actually query and return an array
		$statement->execute();
		$class_list = $statement->fetchAll();
		
		//Now create a list of strings to return that will be outputed
		$class_array = array();
		foreach ($class_list as $class) {
			$class_array[] = 'CS '.$class['pkNumber'] . ' ' . $class['ClassName'] . ' (' . $class['Instructor'] . ')';
		}

		return $class_array;
	}

	private function match_purpose_to_ID($purpose){
		///Make sure we're connected!
		if(!isset($this->connection)){
			die('You must set a database connection before calling functions that use the database!');
		}

		//This function is really simple, query the database for the purpose and return it's ID
		$statement = $this->connection->prepare("SELECT pkPurposeID FROM R332_Purpose WHERE `Purpose` = '$purpose'");
		$statement->execute();

		$result = $statement->fetch();
		return (int)$result['pkPurposeID'];

	}

	public function logUsage(RoomLoginEntry $entry){
		///Make sure we're connected!
		if(!isset($this->connection)){
			die('You must set a database connection before calling functions that use the database!');
		}

		//This function will log a login to the room, so we should authenticate and somewhere around here
		$ID_validator = new UVMValidator($entry);
		$content_validator = new InjectionValidator($entry);

		//Now we begin the checks if anything is wrong we'll return the appropriate status code
		if(!$ID_validator->validate()){
			return self::INVALID_NET_ID;
		}

		if(!$content_validator->validate()){
			return self::INVALID_CONTENT;
		}

		//We make it this far we're alright, the other two properties of RoomLoginEntry are ok because
		//they are populated by the drop downs and things defined by the database itself.
		$statement = $this->connection->prepare( "INSERT INTO R332_Usage (fkPersonID, fkPurposeID, Class, Description ) VALUES ( ?, ?, ?, ? );");
		
		//Bind the values to the query
		$statement->bindValue(1,$entry->getUserID(),      PDO::PARAM_STR);
		$statement->bindValue(2,$this->match_purpose_to_ID($entry->getPurpose()), PDO::PARAM_INT);
		$statement->bindValue(3,$entry->getClassNumber(), PDO::PARAM_STR);
		$statement->bindValue(4,$entry->getContent(),     PDO::PARAM_STR);

		//execute the insertion
		$statement->execute();

		//ENTRY OKAY inherited from parent class
		return self::ENTRY_OKAY;
	}
}
// //Some test code
// echo "making class\n </br>";
 // $testd = RoomSignInDB::getInstance();
// echo "attempting to connect\n </br>";
// $dbc = $testd->connect_to_db();
// //var_dump($testd->getClassList());

 
 
// $entry = new RoomLoginEntry('ejeldrid','doing stuff!', 'Study','CS 000 Bla');
// $testd->logUsage($entry);
// $testd->close_connection_to_db();
?>