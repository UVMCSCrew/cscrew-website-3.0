<?php
/***********************/
/*Ethan J. Eldridge
/*May 27th 2012
/*It's only 4:50am or so 
/*This file contains the Entry class as well as its children
/*The validator objects for these classes can be found in EntryValidation.php
***********************/

// added this so I can tell when the controller includes the model
// echo "model included";
class Entry{
	protected $userID;
	protected $content;

	public function __construct($action, $POST){
		$this->userID = $POST['uvm_id'];
		//$this->$content = $POST['content'];
	}
	public function getUserID(){
		return $this->userID;
	}

	public function getContent(){
		return $this->content;
	}

	public function setContent($content){
		$this->content = $content;
	}
}

//This class extends entry so our validators can operate on it
//It represents an entry that will end up going to the usage log
//in the database, and is created from the login.php page
class RoomLoginEntry extends Entry{
	protected $purpose;
	protected $class;

	//These classes are basically just helpful objects to hold data in C++ I'd use a struct
	public function __construct($action, $POST){
		$this->userID = $POST['uvm_id'];
		$this->content = $POST['content'];
		$this->purpose = $POST['purpose'];
		$this->class = $POST['uvm_class'];
	}

	public function getClassNumber(){	
		//By Convention the class's look like: CS xxx Course Title (Instructor)
		//where xxx is the course number and the pk of the database table
		return substr($this->class, 3,3);
	}

	public function getPurpose(){
		return $this->purpose;
	}
}

//This class represents a forum entry, we haven't started working on
//the forum as of yet, but I assume we'll need to validate for malicious
//code in content or escape it so heres an object that we'll use for that
//(not the validation or escaping that's done by decorator objects, but holding
//the data itself is this classes job. There's a good chance this will get bigger)
class ForumEntry extends Entry{
	protected $upvote_rank;
	protected $parentID;
	protected $postID;

	//I'm assuming we'll auto increment certain things like post ID and such
	public function __construct($action, $POST){
		$this->userID = $POST['uvm_id'];
		$this->content = $POST['content'];
		$this->upvote_rank = 0;
		$this->parentID = $POST['parentPostID'];
		$this->postID = $POST['postID'];
	}

	public function setUpvoteRank($rank){
		$this->upvote_rank = $rank;
	}

	public function getUpvoteRank(){
		return $this->upvote_rank;
	}
}

class WebsiteLogin extends Entry{
	private $password;
	public $priv_lvl;


	public function __construct($POST){
		$this->userID = $POST['uvm_id'];
		$this->password = $POST['password'];
		$this->content = "NONE";
		if(isset($POST['priv_level'])){
			$this->priv_lvl = $POST['priv_level'];	
		}else{
			$this->priv_lvl = 0;
		}		
	}


	public function getPassword(){
		return $this->password;
	}

	public function clearPassword(){
		$this->password = "Correct Horse Battery Staple";
	}




}



?>