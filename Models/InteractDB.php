<?php

// require_once('Configuration/config.php');

// define("DATABASE_NAME", "CSCREW_1");
// define("DATABASE_HOST", "webdb.uvm.edu");
// define("DATABASE_PASS", "uijoexez");
// define("DATABASE_USER", "cscrew");

// $miscData = array('tableKeyName' => 'uvm_id', 'tableKey' => 'jdicker1', 'tableName' => 'Social_Networks', 'twitter' => 'zzzzz', 'facebook' => 'faceeee');
// $try = new InteractDB("action", $miscData);

class InteractDB{
	// as defined in config.php
	private $dbName = DATABASE_NAME;
	private $host = DATABASE_HOST;
	private $dbPass = DATABASE_PASS;
	private $user = DATABASE_USER;
	public $dsn = null;	// for PDO
	
	public $connection = null;

	public $action = null;			// this is the action to be performed on the DB (SELECT INSERT etc)
	public $numberEntries = null;	// this is the number of total items to be acted upon
	private $data = array();		// the array of data to be INSERTED (or whatever)
	public $returnedRows = array();


	function __construct($action, $data){
		$this->dsn = "mysql:dbname=".DATABASE_NAME.";host=".DATABASE_HOST;
		$this->data = $data;
		$this->connection = $this->dbConnect();
		$this->action = $action;
		$this->parseActions();
	}



	private function parseActions(){
		switch($this->action){
			case "insert":
			$this->insertStatement();
			break;

			case "select":
			$this->selectStatement();
			break;

			case "update":
			$this->updateStatement();
			break;
		}

	} // end parseActions



	public function dbConnect(){
		//Connect to the database and select the database		
		try{
			$connection = new PDO($this->dsn, DATABASE_USER, DATABASE_PASS);
			$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			return $connection;
		}catch(PDOException $err){return null;}	// if we cant connect, return null	
	} // end dbConnect


	private function selectStatement(){
		$data = $this->data;
		$connection = $this->connection;
		$numEntries = $this->numberEntries;
		$tableName = $data['tableName'];	// tableName must be specified
		unset($data['tableName']);


		// A temporary array to hold the fields in an intermediate state
		$whereClause = array();

		// Iterate over the data and convert to individual clause elements
		foreach ($data as $key => $value) {
		    $whereClause[] = "$key = :$key";
		}
		// Construct the query
		if (count($whereClause) > 0 ){
			// we have a conditional
			$query = 'SELECT * FROM '.$tableName.' WHERE '.implode(' OR ', $whereClause).'';
		}else{
			// grab all rows from table
			$query = 'SELECT * FROM '.$tableName.';';
		}
		// Prepare the query
		$stmt = $connection->prepare($query);
		// Execute the query
		$stmt->execute($data);
		$this->returnedRows = $stmt->fetchAll();

	} // end selectStatement

	private function insertStatement(){
		$data = $this->data;
		$connection = $this->connection;
		$numEntries = $this->numberEntries;
		$tableName = $data['tableName'];	// tableName must be specified
		unset($data['tableName']);
		$questionMarks = "";;

		// A temporary array to hold the fields in an intermediate state
		$fieldName = array();
		$fieldValue = array();

		// Iterate over the data and convert to individual clause elements
		foreach ($data as $key => $value) {
		    $fieldName[] = "$key,";
		    $fieldValue[] = ":$key,";
		}
		$fieldNames = rtrim(implode($fieldName), ",");
		$fieldValues = rtrim(implode($fieldValue), ",");
		// var_dump($fieldValues);
		// Construct the query
		$query = 'INSERT INTO '.$tableName.' ('.$fieldNames.') VALUES ('.$fieldValues.');';
		var_dump($query);
		// // Prepare the query
		$stmt = $connection->prepare($query);

		// // Execute the query
		$stmt->execute($data);

	} // end insertStatement

	private function updateStatement(){
		// the data array must contain an entry for $data['tableName'] for the SQL WHERE clause
		// the data array must contain an entry for $data['tableKeyName'] for the SQL WHERE clause
		// the data array must contain an entry for $data['tableKey'] for the SQL WHERE clause
		$data = $this->data;
		if(!isset($data['tableName']) || (!isset($data['tableNameKey'])) || (!isset($data['tableKey']))){
			// do error because we don't have enough info to do a proper update
		}else{
		$connection = $this->connection;
		$numEntries = $this->numberEntries;
		$tableName = $data['tableName'];	// tableName must be specified
		$tableKeyName = $data['tableKeyName'];	// tableName must be specified
		$tableKey = $data['tableKey'];
		unset($data['tableKeyName']);
		unset($data['tableKey']);
		unset($data['tableName']);

		// A temporary array to hold the fields in an intermediate state
		$whereClause = array();

		// Iterate over the data and convert to individual clause elements
		foreach ($data as $key => $value) {
		    $whereClause[] = "$key = :$key";
		}
		// Construct the query
		$query = 'UPDATE '.$tableName.' SET '.implode(', ', $whereClause).' WHERE '.$tableKeyName.' ="'.$tableKey.'";';
		// Prepare the query
		$stmt = $connection->prepare($query);
		// Execute the query
		$stmt->execute($data);
		} // end error checking else clause
	} // end insertStatement

} // end InteractDB class

?>