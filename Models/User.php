<?php

 class UserModel{
 	public $dbObj = "null";
	// these vars controll what actions to perform on the user
	public $action = "null";
	private $POST = array();

	// default view associated with this class
	public $view = "User";
	public $vars = array();

	// user attributes
	public $priv_level = 0;
	public $uvm_id = "null";
	public $last_name = "null";
	public $first_name = "null";
	public $username = "null";
	public $personal_url = "null";
	public $git_url = "null";
	public $about_me = "null";
	public $img_url = "noprofile.png";
	public $social = array(
		"facebook" => "null",
		"twitter" => "null",
		"tumblr" => "null",
		"blogger" => "null",
		"rss" => "null",
		"google" => "null",
		"pintrest" => "null",
		"reddit" => "null",
		"myspace" => "null",
		"linkedin" => "null",
		"git" => "null"
		);
	// priv level 0 is for an user without an account
	// priv 1 is a logged in user
	// priv 2 is a cscrew member
	// priv 3 is an admin
	 

	function __construct($POST){
		$this->POST = $POST;
		$this->sortPost($POST);
		$this->createDBObj();
	 }

	function sortPost($POST){
		// assign POST variables
			foreach ($POST as $key => $value){
  				if (isset($POST[$key]))
  				$nameArr = explode("_", $key);
    			$this->social[$nameArr[0]] = $POST[$key];
    			//var_dump($key);
    		}
	}

	function createDBObj(){
		// create an instance of memberDB
		// to pass to its own signUpNewUser method
		// yaay for recursion
		include_once "Models/MemberDB.php";
		$this->dbObj = MemberDB::getInstance();
		$status = $this->dbObj->connect_to_db();
	} // creatDBObj()

	function logIn(){
		// this is where we do all validation.. er get the validation object
		// first we hash the entered password
	 	$salt = time()*0.118321;
	 	$loginName = $this->POST['uvm_id'];
	 	$loginPass = md5($this->POST['password'], $salt);
	 	// package the data in an array because that's what the Entry class expects
		$loginArray = array('uvm_id' => $loginName, 'password' => $loginPass);
		$status = $this->dbObj->connect_to_db();
		if($status == 0){
		// we're connected to the database
			if ($this->dbObj->userExists($loginName) == true){
			// the user exists
				$loginObj = new WebsiteLogin($loginArray);
				if($this->dbObj->checkPassword($loginObj) == true){
					// the password matches the username
					$this->dbObj->logUsage($loginObj);
					return true;
				} else{return false;}
			} else {return false;}
		} else {return false;}
	} // logIn()

	public function retreiveProfile(){
		// takes a user's uvm_id and retreives his
		// profile data
		$uvm_id = $_SESSION['uvm_id'];
		$status = $this->dbObj->connect_to_db();
		if($status == 0){
			return $this->dbObj->getProfileData();
		}


	}

} // UserModel

class NewUser extends Entry{
	// class for creating new users
	public $uvm_id = "null";
	public $username = "null";
	public $password = "null";
	public $dbObj;

	function __construct($POST){
		$this->uvm_id = $POST['uvm_id'];
		$this->username = $POST['uvm_id'];
		$this->password = $this->hash_password($POST['password']);
		$this->createDBObj();
	}

	function hash_password($toBeHashed){
	$salt = time()*0.118321;
	$hashedPassword = md5($toBeHashed, $salt);
	return $hashedPassword;
	} // hash_password()

	function createDBObj(){
		// create an instance of memberDB
		// to pass to its own signUpNewUser method
		// yaay for recursion
		include "Models/MemberDB.php";
		$this->dbObj = MemberDB::getInstance();
		$status = $this->dbObj->connect_to_db();
	} // creatDBObj()

} // NewUser

?>
