
	// ----------- forum controls -----------

	// reply dialog
$(document).ready(function(){

	$('.replyButt').click(function(){
		// perform the animation to slide down the relevant reply box
		var $replyDialog = $(this).parent().parent().find(".responseInput");
		// get the parent comment id from the hidden input 
		var $parent_id_val = $(this).parent().parent().parent().find(".comment_id").val();
		// assign the parent's 
		var $comment_parent_val = $(this).parent().parent().parent().find(".parent_id");
		$comment_parent_val.val($parent_id_val);
		alert($parent_id_val);
		$replyDialog.slideToggle();
	});

	$('.clearButt').click(function(){
		var $clearDialog = $(this).parent().parent().find("textarea");
		$clearDialog.val("");
	});


	// open and close the new post dialog
	$('.newPost').click(function(){
		$('.newPostPop').show();
	});

	$('.closeButton').click(function(){
		$('.newPostPop').hide();
	});
	// end open and close new post dialog


});

	// ----------- end forum controls -----------