<?php

?>
<span class="pageTitle">Forum</span>

<ul class="forum">
	<li class="post">
		<span class="title">How These Will Work</span>
		<span class="userInfo">Posted by: <a href="#">Josh</a> at 12:00pm on 3/16/2012</span>
		<input type="hidden" name="comment_id" class="comment_id" value="420">
		<p>
			These posts will be programatically generated with php after
			the data is pulled from the database. This page is static because
			I am building the css and javascript needed to control the appearance
			once the appearance is complete, we will work on the backend. 

		</p>

		<div class="postButtons">
			<ul>
				<li class="replyButt">reply</li>
				<li>share</li>
			</ul>

			<div class="responseInput">
				<form>
					<textarea rows="3" cols="5" name="reply" tabindex="0"></textarea>
					<input type="hidden" class="parent_id" value="working">
					<input type="submit" value="submit">
					<input type="button" value="clear" class="clearButt">
				</form>
			</div>
		</div>



		<div class="response odd">
			<span class="userInfo">Posted by: <a href="#">Josh</a> at 12:00pm on 3/16/2012</span>
			Yeah That sounds like a good idea, Josh
			<div class="postButtons">
				<ul>
					<li class="replyButt">reply</li>
					<li>share</li>
				</ul>

				<div class="responseInput">
					<form>
						<textarea rows="3" cols="5" name="reply" tabindex="0"></textarea>
						<input type="submit" value="submit">
						<input type="button" value="clear" class="clearButt">
					</form>
				</div>
			</div>
		</div>

		<div class="response odd">
			<span class="userInfo">Posted by: <a href="#">Josh</a> at 12:00pm on 3/16/2012</span>
			Now I'm talking to myself!
			<div class="postButtons">
				<ul>
					<li class="replyButt">reply</li>
					<li>share</li>
				</ul>
			</div>
			<div class="response even">
				<span class="userInfo">Posted by: <a href="#">Josh</a> at 12:00pm on 3/16/2012</span>
				1st response to 2nd response to title message
				<div class="postButtons">
					<ul>
						<li class="replyButt">reply</li>
						<li>share</li>
					</ul>
				</div>
				<div class="response odd">
					<span class="userInfo">Posted by: <a href="#">Josh</a> at 12:00pm on 3/16/2012</span>
					1st response to 2nd response to title message
					<div class="postButtons">
						<ul>
							<li class="replyButt">reply</li>
							<li>share</li>
						</ul>
					</div>
					<div class="response even">
						<span class="userInfo">Posted by: <a href="#">Josh</a> at 12:00pm on 3/16/2012</span>
						1st response to 2nd response to title message
						<div class="postButtons">
							<ul>
								<li class="replyButt">reply</li>
								<li>share</li>
							</ul>
						</div>
					</div>
				</div>
			</div>				
			<div class="response even">
				<span class="userInfo">Posted by: <a href="#">Josh</a> at 12:00pm on 3/16/2012</span>
				Yeah That sounds like a good idea, Josh

				<div class="postButtons">
					<ul>
						<li class="replyButt">reply</li>
						<li>share</li>
					</ul>
					<div class="responseInput">
						<form>
							<textarea rows="3" cols="5" name="reply" tabindex="0"></textarea>
							<input type="submit" value="submit">
							<input type="button" value="clear" class="clearButt">
						</form>
					</div>
				</div>
			</div>
		</div>
	</li>
	<li class="post">
		<span class="title">Post Title</span>
		<span class="userInfo">Posted by: <a href="#">Josh</a> at 12:00pm on 3/16/2012</span>
		<p>
			2nd Title message
			Lorem ipsum dolor sit amet, consectetur 
			adipiscing elit. Donec aliquet auctor velit 
			a consectetur. Cras velit erat, congue in 
			sagittis non, congue non lorem. Phasellus 
			fringilla venenatis magna, at scelerisque 
			odio rhoncus vitae. Aliquam aliquam tincidunt 
			erat, in consequat risus congue non. Lorem 
			ipsum dolor sit amet, consectetur adipiscing 
			elit. Fusce tempor interdum condimentum. Aliquam 
			ultrices, erat dignissim condimentum congue, 
			diam nibh pulvinar dui, vitae consequat diam 
			lacus sit amet metus. Aliquam erat volutpat.
		</p>

		<div class="postButtons">
			<ul>
				<li class="replyButt">reply</li>
				<li>share</li>
			</ul>
		</div>

		<div class="response odd">
				1st response to 2nd title message
			<div class="postButtons">
				<ul>
					<li class="replyButt">reply</li>
					<li>share</li>
				</ul>
			</div>
		</div>

		<div class="response odd">
			2nd response to title message
			<div class="postButtons">
				<ul>
					<li class="replyButt">reply</li>
					<li>share</li>
				</ul>
			</div>
			<div class="response even">
				1st response to 2nd response to title message
				<div class="postButtons">
					<ul>
						<li class="replyButt">reply</li>
						<li>share</li>
					</ul>
				</div>
				<div class="response odd">
					1st response to 2nd response to title message
					<div class="postButtons">
						<ul>
							<li class="replyButt">reply</li>
							<li>share</li>
						</ul>
					</div>
					<div class="response even">
						1st response to 2nd response to title message
						<div class="postButtons">
							<ul>
								<li class="replyButt">reply</li>
								<li>share</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</li>
</ul>