<?php

?>

<div class="signUp">
	<ul>
		<li class="leftCell cell">
			<form method="post" action="<? echo BASEDIR; ?>User/?newUser=true">
				<ul>
					<li>UVM ID</li>
					<li><input type="text" name="uvm_id"></li>
					<li>Desired Username</li>
					<li><input type="text" name="username"></li>
					<li>Password</li>
					<li><input type="password" name="password" id="pass1" class="passwrd"><img class="validInput" src="<? echo BASEDIR; ?>Views/images/bad-x.png" height="20"></li>
					<li>Verify Password</li>
					<li><input type="password" name="passwordVerify" id="pass2" class="passwrd"><img class="validInput" src="<? echo BASEDIR; ?>Views/images/bad-x.png" height="20"></li>
					<li class="buttonRow"><input type="submit" id="submitButt" value="submit" class="button" disabled="disabled"><input type="button" value="clear" id="clear" class="button"></li>
				</ul>
			</form>
		</li>
		
		<li class="rightCell cell">
			<ul>
				<li>* You must have a valid UVM ID to sign up</li>
				<li>* You must choose a password that has at least 6 characters</li> 
			</ul>
		</li>

	</ul>

</div>

<script type="text/javascript">
	 var goodSRC = <? echo "'".BASEDIR. "Views/images/good-check.png'"; ?>;
	 var badSRC = <? echo "'".BASEDIR. "Views/images/bad-x.png'"; ?>;
	 $(document).ready(function(){
		$('.passwrd').keyup(function(){
			var passLength = $('#pass1').val().length;
			if ($('#pass1').val() == $('#pass2').val()){
				if (passLength >= 6){
					$('.validInput').attr('src', goodSRC);
					$('#submitButt').removeAttr("disabled");
				}
				else{
					$('.validInput').attr('src', badSRC);
					$('#submitButt').attr('disabled', "disabled");
				}
			}
			else{
				$('.validInput').attr('src', badSRC);
				$('#submitButt').attr('disabled', "disabled");
			}
		});
	});
</script>