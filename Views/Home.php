<?php
if (!headers_sent()){
  header('Content-Type: text/html');
}

if (!isset($_SESSION['priv'])){
  $_SESSION['priv'] = 0;
}

// defailt to the about page
if (!isset($path) || $path == ""){
// $path = "Views/about.php";
}

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta http-equiv="content-type" content="text/html;charset=utf-8" />
  <meta name="viewport" content="width=device-width" />
  <meta name="author" content="TheWebCrew" />
  <meta name="description" content="UVM-CS-CREW"/>
  <title>CS Crew : Home</title>
  <link rel="shortcut icon" href="http://www.uvm.edu/www/images/templates/favicon.ico" />
  
<!-- these are all stylesheets -->
  <link rel="stylesheet/less" href="<?  echo BASEDIR; ?>Views/css/bootstrap.css" />
<!-- responsive -->
  <link rel="stylesheet/less" media="screen" href="<?  echo BASEDIR; ?>Views/less/custom.less" />
  <link rel="stylesheet/less" media="screen" href="<?  echo BASEDIR; ?>Views/less/forum.less" />
  <!-- 
  <link rel="stylesheet/less" media="screen and (max-width: 800px)" href="<?  echo BASEDIR; ?>Views/less/mobile.less" />
  <link rel="stylesheet/less" media="screen and (max-width: 964px) and (min-width: 800px)" href="<?  echo BASEDIR; ?>Views/less/960.less" />
  <link rel="stylesheet/less" media="screen and (max-width: 1260px) and (min-width: 965px)" href="<?  echo BASEDIR; ?>Views/less/1260.less" />
  -->
  <link href="<?  echo BASEDIR; ?>Views/css/default.css" rel="stylesheet" type="text/css" />
  <link href="<?  echo BASEDIR; ?>Views/css/fileuploader.css" rel="stylesheet" type="text/css" />
  <link href="<?  echo BASEDIR; ?>Views/js/Jcrop/jquery.Jcrop.css" rel="stylesheet" type="text/css" />


<!-- These are all plugins and libraries -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.js"></script><!-- jquery javascript library --> 
  <script type="text/javascript" src="<? echo BASEDIR; ?>Views/js/less.js"></script> <!-- less css framework -->
  <script type="text/javascript" src="<? echo BASEDIR; ?>Views/js/bootstrap-dropdown.js"></script> <!-- twitter bootstrap -->
  <script type="text/javascript" src="<? echo BASEDIR; ?>Views/js/bootstrap-collapse.js"></script>
  <script type="text/javascript" src="<? echo BASEDIR; ?>Views/js/galleria/galleria-1.2.7.min.js"></script><!-- photo gallery plugin -->
  <script type="text/javascript" src="<? echo BASEDIR; ?>Views/js/jquery.easing.1.3.js"></script><!-- bounce plugin for jquery javascript library -->  
 
<!-- These are for the slick profile image uploader --> 
  <script type="text/javascript" src="<? echo BASEDIR; ?>Views/js/jquery-impromptu.js"></script>
  
  <script type="text/javascript" src="<? echo BASEDIR; ?>Views/js/Jcrop/jquery.Jcrop.min.js"></script>
  <script type="text/javascript" src="<? echo BASEDIR; ?>Views/js/jquery-uberuploadcropper.js"></script>

<!-- heres our custom JS file  -->
  <script type="text/javascript" src="<? echo BASEDIR; ?>Views/js/index.js"></script>

<!-- Print stylesheet at the bottom to override less styless  -->
  <link rel="stylesheet" media="print" href="<?  echo BASEDIR; ?>Views/css/print.css" />

<!-- include analytics -->
<!--   <? include "Views/analytics.php"; ?> -->

</head>

<body>
<!-- <div id="codeOutput" style="background-color:#fff">
</div> -->
  
<div id="topBar">
    <!-- this is where all of our top-level nav goes, along with logo and title --> 
  <a href=<? echo "'".BASEDIR."'";?>><img alt="logo" src="<? echo BASEDIR; ?>Views/images/logo.png" class="uvmLogo"></a>
    <!--  the navbar at the top/middle of the screen -->
    <!--  Currently set to use ajax loads, but will be replaced w/ -->
    <!--  PHP loads once URL resolution is built --> 
  <ul class="mainNav">
      <li class="homeLink">
        <a class="navLinks homeLink" href=<? echo "'".BASEDIR."/'";?>>
          <div class="utf8 left"><!--[if !IE]> -->&#10094;<!-- <![endif]--></div>
          Home
          <div class="utf8 right"><!--[if !IE]> -->&#10095;<!-- <![endif]--></div>
        </a>
      </li>             
      <li>
        <a class="navLinks projectsLink" href=<? echo "'".BASEDIR."projects/'";?>>
          <div class="utf8 left"><!--[if !IE]> -->&#10094;<!-- <![endif]--></div>
          Projects
          <div class="utf8 right"><!--[if !IE]> -->&#10095;<!-- <![endif]--></div>
        </a>
      </li>
      <li id="calendarLink">
        <a class="navLinks calendarLink" href=<? echo "'".BASEDIR."calendar/'";?>>
          <div class="utf8 left"><!--[if !IE]> -->&#10094;<!-- <![endif]--></div>
          Calendar
          <div class="utf8 right"><!--[if !IE]> -->&#10095;<!-- <![endif]--></div>
        </a>
      </li>
      <li>
        <a class="navLinks membersLink" href=<? echo "'".BASEDIR."members/'";?>>
          <div class="utf8 left"><!--[if !IE]> -->&#10094;<!-- <![endif]--></div>
          Members
          <div class="utf8 right"><!--[if !IE]> -->&#10095;<!-- <![endif]--></div>
        </a>
      </li>
      <li>
        <a class="navLinks helpLink" href=<? echo "'".BASEDIR."help/'";?>>
          <div class="utf8 left"><!--[if !IE]> -->&#10094;<!-- <![endif]--></div>
          Get Help
          <div class="utf8 right"><!--[if !IE]> -->&#10095;<!-- <![endif]--></div>
        </a>
      </li>
      <li>
        <a class="navLinks contactLink" href=<? echo "'".BASEDIR."contact/'";?>>
          <div class="utf8 left"><!--[if !IE]> -->&#10094;<!-- <![endif]--></div>
          Contact
          <div class="utf8 right"><!--[if !IE]> -->&#10095;<!-- <![endif]--></div>
        </a>
      </li>
  </ul>

  <!-- account management dropdown, styled with bootstrap -->
  <div class="account">
          <ul class="accountManage">
        <!-- account dropdown -->
          <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="path/to/page.html">
              Account 
                <b class="caret"></b>
            </a>
              <ul class="dropdown-menu">
                  <? if($_SESSION['priv']<1){echo "<li class='login'><a href='". BASEDIR . "login/'>Log in</a></li>";}?>
                  <? if($_SESSION['priv']<1){echo "<li class='login'><a href='". BASEDIR . "signup/'>Sign Up</a></li>";}?>
                  <? if($_SESSION['priv']>=1){echo "<li class='logout'><a href='".BASEDIR."User/?logOut=yes'>Log out</a></li>";}?>
                  <? if($_SESSION['priv']>=1){echo "<li class='divider'></li>";}?>
                  <? if($_SESSION['priv']>=1){echo "<li><a href="."'".BASEDIR."User/?getProfile=".$_SESSION['uvm_id']."'>Profile <i class='icon-user'></i></a></li>";}?>
                  <? if($_SESSION['priv']>=1){echo "<li><a href="."'".BASEDIR."User/?getProfile=".$_SESSION['uvm_id']."&page=Editprofile'>Settings</a></li>";}?>
                  <? if($_SESSION['priv']>=1){echo "<li class='divider'>";}?>
                  <? if($_SESSION['priv']>=1){echo "<li><a href='".BASEDIR."Forum/?getPosts=yes'>Forum</a></li>";}?>
                  <? if($_SESSION['priv']>=1){echo "<li><a href='".BASEDIR."messages/'>Messages</a></li>";}?>
                 
              </ul>
          </li>
        </ul>
  </div>
</div>  <!-- end topbar -->


<div id="mainContain">
     <!--this is where we load all of our main modules. Think forums, calendars, forms, blog posts, about us etc 
   -->
   <?  
   include $path; 
   if (isset($debug)){
    echo $debug;
  }
  ?>
</div>


<!-- </body> -->
</html>
