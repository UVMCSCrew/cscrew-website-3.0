<?php
	$projectName = "Some Game";
	$projectURL = "http://www.github.com";
	$team = array("Joshua Dickerson", "Ethan Eldridch", "Scott MacGewan");
	$readmeURL = 'https://raw.github.com/JoshuaDickerson/Vroomba/master/README.txt';
	$projectDescription = file_get_contents($readmeURL);
?>

<div id="projectProfile">
	<ul>
		<li class="left">
			<table>
				<tr>
					<td><span class="contentHeader"><? echo $projectName; ?></span></td>
				</tr>
				<tr>
					<td>Project URL:</td><td><? echo $projectURL; ?> </td>
				</tr>

				<tr>
					<td>Project Leader:</td><td><? echo $team[0]; ?> </td>
				</tr>

				<tr>
					<td>Other Members</td><td></td>
				</tr>
			</table>
		</li>

		<li class="right">
			<span class="contentHeader">Description</span>
				<? echo $projectDescription; ?> 
		</li>
	</ul>
</div>

