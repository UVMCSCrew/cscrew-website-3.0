<?php
// echo "View.php included";
class View{
	public $view = "default template";
	public $modelObj = "null";
    public $modelVars = array();

	function __construct($modelObj){
        $this->modelObj = $modelObj;
        $this->view = $modelObj->view;
        if(isset($modelObj->vars)){
            $this->getVars();
        }
        $this->display();
	}



	public function display(){
        if(file_exists('Views' . '/' . $this->view . '.php')){
            $vars = $this->modelVars;
            $path = 'Views' . '/' . $this->view . '.php';
        }
        else{
            $path = "Views/About.php";
        }
        
        if($this->view != "xml"){
            include "Views/Home.php";
        }else{
            include "Views/XMLHome.php";
        }
        // var_dump($path);
	}

    public function getVars(){
        $this->modelVars = $this->modelObj->vars;

    }
	

}

?>