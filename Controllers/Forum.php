<?php

class Forum{
 	// this is the controller for the ForumModel model
 	// this class takes a user's post data, and the actions to be performed
 	// and performs them
 	private $POST = array();
 	private $actions = array();
 	public $view = "Threads";
 	public $modelObj = "null";

 	function __construct($actions, $POST){
		$this->POST = $POST;
	 	$this->actions = $actions;
	 	include_once "Models/Forum.php";
		$forumObj = new ForumModel($this->actions, $this->POST);
		$this->modelObj = $forumObj;
	 	$this->parseAction($this->actions);
	 }




	function parseAction($actions){
		// takes the actions to be performed on the 
		// controller and perfomrs them if they exist
		$children = array_keys($actions);
		$methods = array_values($actions);

		if(count($children) != count($methods)){
			// if there are a different number of actions than variables
			// throw an error
			// please add my functionality
		}
		else{
			foreach($children as $value){
				// as long as there are an equal number of methods and variables
				// do --> for every action perform the switch statement
				switch ($value) { 
				    case "comment":				       	
				       	// what to do with comment ?
				       	switch($this->actions['comment']){ // begin nested switch
				       		case "newComment":
				       		$commentResult = $this->modelObj->newComment();
				       		if($commentResult != true){ /* do error */ }
				       		break;

				       		case "deleteComment":
				       		$commentResult = $this->modelObj->deleteComment();
				       		if($commentResult != true){ /* do error */ }
				       		break;

				       		case "editComment":
				       		$commentResult = $this->modelObj->editComment();
				       		if($commentResult != true){ /* do error */ }
				       		break;

				       	} // end nested switch
				       	
				    case "newPost":
					    $commentResult = $this->modelObj->newPost();
					    // var_dump($commentResult);
					    if($commentResult != true){ /* do error */ }
					    $this->view = $this->modelObj->view;
				    break;

				    case "getPosts":
				    $this->modelObj->getPosts();
				    break;

				    case "output":
				    $this->modelObj->view = "xml";
				    break;

				       	

				        

				    default:
				       // echo "i is not equal to 0, 1 or 2";
				}
			}
		}

	}

}


?>