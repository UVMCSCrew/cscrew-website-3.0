<?php
require_once('Models/Entry.php');

 //echo "User.php included";
 class User{
 	// this is the controller for the UserModel model
 	// this class takes a user's post data, and the actions to be performed
 	// and performs them
 	private $POST = array();
 	private $actions = array();
 	public $view = "about.php";
 	public $modelObj = "null";

 	function __construct($actions, $POST){
		$this->POST = $POST;
	 	$this->actions = $actions;
	 	$this->parseAction($this->actions);
	 }

	function parseAction($actions){
		// takes the actions to be performed on the 
		// controller and perfomrs them if they exist
		$children = array_keys($actions);
		$methods = array_values($actions);

		if(count($children) != count($methods)){
			// if there are a different number of actions than variables
			// throw an error
			// please add my functionality
		}
		else{
			foreach($children as $value){
				// as long as there are an equal number of methods and variables
				// do --> for every action perform the switch statement
				switch ($value) {
				    case "logIn":
				    	// the user is attempting to log in
				    	include_once "Models/User.php";
				       	$userObj = new UserModel($this->POST);
				       	$this->modelObj = $userObj;
				       	if($userObj->logIn() == true){
				       		// the login worked, sett the user id session variable
				       		$_SESSION['uvm_id'] = $this->POST['uvm_id'];
				       		// drop the user into his profile
				       		$profileArray = $userObj->retreiveProfile();
				        	$this->modelObj->vars = $profileArray;
				       		$this->modelObj->view = "Profile";

				       	}
				       	else{
				       		// the login failed
				       	}
				        break;

				    case "newUser":
				    	// the user is attempting to create an account
				    	include_once "Models/User.php";
				        $newUser = new NewUser($this->POST);
				        $this->modelObj = $newUser;
				        $result = $newUser->dbObj->signUpNewUser($newUser);
				        if ($result == 0){
				        	$_SESSION['priv'] = 1; 
				        	$_SESSION['uvm_id'] = $newUser->uvm_id;
				        	$this->modelObj->view = "Editprofile";
				        }
				        else{
				        	// the account creation failed
				        }
				        break;

				    case "logOut":
				        $this->logOut();
				        break;

				    case "updateProfile":
				    	// called from Views/Editprofile.php
				   		include_once "Models/User.php";
				        $userObj = new UserModel($this->POST);
				        $this->modelObj = $userObj;
				        $result = $userObj->dbObj->editProfile($userObj);
				        if ($result == 0){
				        	$profileArray = $userObj->retreiveProfile();
				        	$this->modelObj->vars = $profileArray;
				        	$this->modelObj->view = "Profile";
				        }
				        else{
				        	$this->modelObj->view = "Editprofile";
				        }
				    	break;

				    case "getProfile":
				    	include_once "Models/User.php";
				    	// check if the user is logged in and the session is set
				    	if (isset($_SESSION['uvm_id']) && $_SESSION['uvm_id'] == $methods[0]){
				        	$userObj = new UserModel($methods);
				        	$this->modelObj = $userObj;
					        $profileArray = $userObj->retreiveProfile();
					        $this->modelObj->vars = $profileArray;
					        if(isset($actions['page']) && $actions['page'] == "Editprofile"){
					        	$this->modelObj->view = "Editprofile";
					        }else{
					        	$this->modelObj->view = "Profile";
					        }
				        }
				        // if not, do a full logout
				        else {
				        	$this->logOut();
				        	// doError("you dont have permission for that");
				        }
				        break;

				    default:
				       // echo "i is not equal to 0, 1 or 2";
				}
			}
		}

	}

	 function logOut(){
	 	// empty the array
	 	$x = new stdClass();
	 	$this->modelObj = $x;
	 	$this->modelObj->view = "about";
	 	$_SESSION = array();
	 	// kill the cookies
		if (ini_get("session.use_cookies")) {
		    $params = session_get_cookie_params();
		    setcookie(session_name(), '', time() - 42000,
		        $params["path"], $params["domain"],
		        $params["secure"], $params["httponly"]
		    );
		}
		// destroy the session for good measure
		session_destroy();
	 }

	 private function setSession($vars){
	 	$_SESSION['uvm_id'] = $vars['uvm_id'];
	 	$_SESSION['priv_lvl'] = $vars['priv_lvl'];
	 }


}





?>
