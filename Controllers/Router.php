 <?php
//$routerObj = new Router($_SERVER);
// var_dump($routerObj);
 class Router{
 	public $requestURL = "defaultTitle";
 	public $userAgent  = "defaultTitle";
 	public $clientIP = "default client IP";
 	public $deviceType = "desktop";
 	public $controller = "default Controler requested";
 	public $model = "defailt model requested";
 	public $action = array();


 	function __construct($serverRequest){
 		$this->requestURL 	= $serverRequest['REQUEST_URI'];
 		$this->userAgent 	= $serverRequest['HTTP_USER_AGENT'];
 		$this->clientIP		= $serverRequest['REMOTE_ADDR'];
 		$this->deviceType 	= $this->getDeviceType();
 	}

 	function parseURL(){
 		$requestArr = parse_url($this->requestURL); // takes the URL and parses it into an array indexed by (scheme, host, user, password, path, query) 		
 		$urlPath = explode("/", $requestArr['path']); //  this is for parsing urls like /users/ tells us which class we need
		if (isset($requestArr['query'])){
			$urlQuery = explode("&", $requestArr['query']); // this is for parsing urls like ?action=logout
			$numQuery = count($urlQuery);
		}
		$actionQueried = array();

		//iterate through our query, assigning actions to the action array
		
		if (isset($urlQuery)){
			foreach($urlQuery as $value){
				$splode = explode("=", $value);
				$actionQueried[$splode[0]] = $splode[1];
			}
		}

		// the following trims the leading and following "/" off of the path
		$dirTrim = substr_replace(BASEDIR ,"",-1);
		$dirTrim = substr_replace($dirTrim ,"",0,1);

		// all our model file names are like User.php. 
		// this takes the model query, lowecases the whole thing, then uppercases the first letter.
		// always resulting in the appropriate model name format.
		
		if(isset($urlPath[2+array_search($dirTrim, $urlPath)])){
			$modelQuery = $urlPath[2+array_search($dirTrim, $urlPath)];
			$this->model = ucfirst(strtolower($modelQuery));
		}
		if(isset($urlPath[1+array_search($dirTrim, $urlPath)])){
			$ControllerQuery = $urlPath[1+array_search($dirTrim, $urlPath)];
			$this->controller = ucfirst(strtolower($ControllerQuery));
		}
				
		$this->action = $actionQueried;
	}


 	function getDeviceType(){
 	// this function tells us whether its a phone or not
 	// not sure if this should be in the router or the display
 		if(stristr($this->userAgent, 'mobile') || stristr($this->userAgent, 'android') || stristr($this->userAgent, 'iphone') || stristr($this->userAgent, 'ipod')){
 			return 'mobile';
 		}
 		else {
 			return 'desktop';
 		} 
 	}
}

?>