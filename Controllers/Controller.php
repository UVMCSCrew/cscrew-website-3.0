
<?php

// Joshua Dickerson
// Developed 2012 at The University of Vermont


// head controller class
// this class creates an instance of a router
// which parses the URL and delivers to the controller
// a path to a model file and an array of actions to operate
// on that model

// Made by Joshua Dickerson for UVM-CSCREW
// joshuajdickerson@gmail.com

// URLs look like /basedir/model/?action=doAction

include "Configuration/config.php";

// recieves the url, instanciates the appropriate controller -->
$controllerObj = new Controller($_SERVER, $_POST);

class Controller{
	public $routerObj ="default obj";
	public $modelObj = "default object";

 	public $model = "About";	// this is the default
 	public $controller = "";
 	public $view = "default view";

 	private $POST = array();
 	private $SERVER = array();

 	// first thing the controller needs to do is fire up the router, which parses 
 	// the requested URL into its component parts 
 	function __construct($SERVER, $POST){
 		$this->POST = $POST;	// grab all the $POST data for local use
 		$this->SERVER = $SERVER;	// grab all $SERVER data to be used by the router
 		require "Controllers/View.php";
		require "Controllers/Router.php";
 		$this->buildDisplay();
 	}

	public function buildDisplay(){
		// build the router
		$this->routerObj = new Router($this->SERVER);
		// gets our model object
		$this->modelObj = $this->getModelData();
		
		// creates an instance of the view object, passes in the model
		$view = new View($this->modelObj);
	}

 	// second thing is to create a new model object defined by the url/router
 	// we then get the applicable model data, and drop it into an array
	function getModelData(){
		// get data from the model, returns the model object
		// get array of model and action from the routerObj
		$this->routerObj->parseURL();
			// if the router has created a controller instance, and that instance isn't empty?
		if (isset($this->routerObj->controller) && $this->routerObj->controller != ""){
			// if there is an associated controller, make an instance of that object
			// perform the requested action, and return the data, otherwise drop into 
			// the default model.  
			if(file_exists("Controllers/".$this->routerObj->controller.".php")){ // check if a file exists in the controller dir
				$this->controller = $this->routerObj->controller;	// grab the controller object from the router
				include "Controllers/".$this->controller.".php";	// include the controller file
				$controllerObj = new $this->controller($this->routerObj->action, $this->POST);	// create an instance of the controller
			 	$modelObj = $controllerObj->modelObj;	// use the controller to generate an instance of the paired model object
			 	return $modelObj;
			}else{	// a controller was called, but does not exist, send user to the default
				include "Models/DefaultModel.php";
				$modelObj = new DefaultModel($this->routerObj->controller);
				return $modelObj;
			} // end nested if-else
		}
		else{// this else accounts for the baseline url.. i.e. www.website.com (there is no controller defined)
			include "Models/DefaultModel.php";
			$modelObj = new DefaultModel("about");
			return $modelObj;
		} // end outer if-else

	} // end getModelData()

} // end Controller class

?>
